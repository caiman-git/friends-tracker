import time
import datetime

DB_CONFIG = {
    'dbname': 'friends_tracker',
    'user': 'friends_tracker',
    'host': 'localhost',
    'password': ''
}

TEST_DB_CONFIG = {
    'dbname': 'test_database',
    'user': 'friends_tracker',
    'host': 'localhost',
    'password': ''
}

PWD_SALT = '8736ksjadgfsgGHGGUJG886'
REF_SALT = 'jjut5978skdjvbJHBHJBVH9'

TEST_FIXTURE_PATH = 'fixtures/db.sql'

INITIAL_IMPORT = False


def hash(*args):
    import hashlib

    check_string = PWD_SALT + ':'.join(map(str,args))
    return hashlib.sha224(check_string).hexdigest()


def refcode(*args):
    import hashlib

    check_string = REF_SALT + ':'.join(map(str,args))
    return hashlib.sha224(check_string).hexdigest()


def now_time():
    return time.mktime(datetime.datetime.now().timetuple())


def month():
    return time.mktime((datetime.datetime.now()+datetime.timedelta(days=30)).timetuple())
