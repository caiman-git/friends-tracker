from models import MODELS
import urllib
import json


class response(object):
    def __init__(self, data=None):
        self.data = data

    def as_data(self):
        return self.data

    def as_json(self):
        return json.dumps(self.data)


class header:
    plain = ('Content-Type', 'text/plain')
    html = ('Content-Type', 'text/html')
    app_json = ('Content-Type', 'application/json')


class http_request(object):
    def __init__(self, environment):
        self.environment = environment
        self.status = '200 OK'
        self.headers = [header.app_json]
        self.content = response()

    def process_path(self, path):
        url = [piece for piece in path.split('/') if piece is not '']

        app, model, method = tuple(url)
        return model, method

    def process_args(self):
        if self.environment['REQUEST_METHOD'] == 'POST':
            return self.process_post()
        return self.process_get()

    def process_post(self):
        body_size = int(self.environment.get('CONTENT_LENGTH', 0))
        body = self.environment['wsgi.input'].read(body_size)
        args = dict((arg[0], arg[1]) for arg in [piece.split('=') for piece in body.split('&')])
        return args

    def process_get(self):
        self.environment['QUERY_STRING'] = urllib.unquote_plus(self.environment['QUERY_STRING'])
        args = {}
        if len(self.environment['QUERY_STRING']) > 0:
            args = dict((arg[0], arg[1]) for arg in [piece.split('=') for piece in self.environment['QUERY_STRING'].split('&')])
        return args

    def process_url(self):
        model, method = self.process_path(self.environment['PATH_INFO'])
        args = self.process_args()
        # args.pop('token', 0);

        self.content.data = self.make_response(model, method, args)

    def make_response(self, model, method, args):
        if args:
            return getattr(MODELS[model], method)(args)
        return getattr(MODELS[model], method)()

    def response(self):
        return self.content.as_json()
