import settings
import psycopg2
import psycopg2.extras
import datetime
import time


def get_connection():
    """
    Getting psycopg2 connection object.

    :rtype connection object
    :raise Exception
    """
    try:
        connection = psycopg2.connect(**settings.DB_CONFIG)
        connection.autocommit = True
        return connection
    except:
        raise Exception('Unable to connect')

def get_test_connection():
    try:
        connection = psycopg2.connect(**settings.TEST_DB_CONFIG)
        connection.autocommit = True
        return connection
    except:
        raise Exception('Unable to connect')


def get_cursor():
    """
    Getting psycopg2.connection cursor

    :rtype cursor object
    :raise Exception
    """
    connection = get_connection()
    return connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

def get_test_cursor():
    connection = get_test_connection()
    return connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)


class sql(object):
    """
    Class created to represent DML statements
    """
    get_fields = ('SELECT a.attname as "field",'
                  ' pg_catalog.format_type(a.atttypid, a.atttypmod) as "type"'
                  ' FROM pg_catalog.pg_attribute a WHERE a.attnum > 0'
                  ' AND NOT a.attisdropped AND a.attrelid ='
                  ' (SELECT c.oid FROM pg_catalog.pg_class c'
                  ' LEFT JOIN pg_catalog.pg_namespace n'
                  ' ON n.oid = c.relnamespace WHERE c.relname ~ \'^({})$\''
                  ' AND pg_catalog.pg_table_is_visible(c.oid));')

    select_all = 'SELECT * FROM "{}" WHERE is_deleted = 0;'
    select_param = 'SELECT * FROM "{0}" WHERE ({1}) AND is_deleted = 0;'
    select_param_fr = 'SELECT * FROM "{0}" WHERE (({1}) AND is_deleted = 0) OR (({1}) AND is_deleted = 1);'
    select_filter = 'SELECT {0} FROM "{1}" WHERE ({2}) AND is_deleted = 0;'
    delete = 'UPDATE "{0}" SET is_deleted=1 WHERE {1} returning id;'
    recover = 'UPDATE "{0}" SET is_deleted=0, created_at=\'{2}\' WHERE {1} returning id;'
    insert = 'INSERT INTO "{0}" ({1}) VALUES ({2}) returning id;'
    update = 'UPDATE "{0}" SET {1} WHERE id={2} returning id;'

    # updates = 'SELECT * FROM "{0}" WHERE ({1}) AND {2} >= \'{3}\' AND is_deleted = 0;'
    updates_created = 'SELECT * FROM "{0}" WHERE ({1}) AND is_deleted = 0 AND created_at >= \'{3}\';'
    updates_updated = 'SELECT * FROM "{0}" WHERE ({1}) AND is_deleted = 0 AND updated_at >= \'{3}\' AND created_at < \'{3}\';'
    updates_deleted = 'SELECT * FROM "{0}" WHERE ({1}) AND is_deleted = 1 AND updated_at >= \'{3}\' AND created_at < \'{3}\';'

    get_coords = ('select distinct'
                  '    coordinate.latitude as lat,'
                  '    coordinate.longitude as long,'
                  '    "user".id as user_id '
                  'from coordinate join'
                  '    "user" on coordinate.user_id = "user".id join'
                  '    zone on zone.user_id = "user".id join'
                  '    zone_group on zone_group.zone_id = zone.id join'
                  '    "group" on zone_group.group_id = "group".id join'
                  '    member on member.group_id = "group".id '
                  'where "user".id in ('
                  '    select invited from friends where inviter = %s'
                  '    union'
                  '    select inviter from friends where invited = %s'
                  ') '
                  'and coordinate.user_id = "user".id '
                  'and coordinate.longitude < zone.br_longitude '
                  'and coordinate.longitude > zone.tl_longitude '
                  'and coordinate.latitude > zone.tl_latitude '
                  'and coordinate.latitude < zone.br_latitude;')
    get_coords2 = ('select '
                   '    "user".id,'
                   '    coordinate.longitude as longitude, '
                   '    coordinate.latitude as latitude '
                   'from "user" join '
                   'coordinate on "user".id = coordinate.user_id '
                   'where %s = any("user".watchers);')


def get_fields_with_types(table):
    """
    Getting table fields with types in PostgreSQL SQL dialect format.

    :param table: table name
    :type table: basestring
    :rtype dict
    :raise Exception
    """

    cursor = get_cursor()
    cursor.execute(sql.get_fields.format(table))
    return cursor.fetchall()


class manager(object):
    """
    Manager class is adapter for models methods to database tables.
    Generate queries according to incoming params.
    """
    def __init__(self):
        """
        The initializer
        """

        self.model = None
        # model name would be represented as __name__ of Model class
        self.base_model = None
        self.cursor = get_cursor()

    def _fetchall(self, statement):
        self.cursor.execute(statement)
        data = self.cursor.fetchall()
        if data:
            for i in data:
                for k, v in i.iteritems():
                    if isinstance(v, datetime.datetime):
                        i[k] = time.mktime(v.timetuple())
        return data

    def _prepare_data(self, data):
        for k, v in data.iteritems():
            if isinstance(v, datetime.datetime):
                data[k] = time.mktime(v.timetuple())
        return data

    def _prepare_bulk_data(self, data):
        for i in data:
            i = self._prepare_data(i)
        return data

    def _insert(self, statement, values):
        self.cursor.execute(statement, values)
        return self.cursor.fetchall()

    def _placeholders(self, count):
        return ('%s, '*count)[:-2]

    def _check_params(self, params):
        for param in params:
            if param not in self.base_model.required_fields:
                return False, param
            return True, self.base_model.get_fields()

    def _prepare_args(self, params):
        return ['%s=\'%s\'' % (k, v) for k, v in params.items()]

    def updates(self, params, condition, timestamp, deleted=False):
        timestamp = datetime.datetime.fromtimestamp(float(timestamp))
        params = ' AND '.join(self._prepare_args(params))

        if deleted:
            self.cursor.execute(
                sql.updates_deleted.format(
                    self.model,
                    params,
                    condition,
                    timestamp
                )
            )
            return self._prepare_bulk_data(self.cursor.fetchall())

        self.cursor.execute(
            sql.updates_created.format(
                self.model,
                params,
                condition,
                timestamp
            )
        )
        created = self._prepare_bulk_data(self.cursor.fetchall())

        self.cursor.execute(
            sql.updates_updated.format(
                self.model,
                params,
                condition,
                timestamp
            )
        )
        updated = self._prepare_bulk_data(self.cursor.fetchall())

        return created, updated

    def create(self, params):
        status, msg = self._check_params(params)

        if status == False:
            return {'error': 1, 'message': 'missing {}'.format(msg)}

        args = self._prepare_args(params)
        exists = self._fetchall(
            sql.select_param_fr.format(self.model, ' AND '.join(args))
        )

        if len(exists) > 0:
            if exists[0]['is_deleted'] == 1:
                self.cursor.execute(sql.recover.format(self.model, ' AND '.join(args), datetime.datetime.fromtimestamp(settings.now_time())))
                self.cursor.execute(sql.select_param.format(self.model, ' AND '.join(args)))
                return {'error': 0, 'added': self._prepare_data(self.cursor.fetchall()[0])}
            else:
                return {'error': 1, 'message': '{} already exists'.format(self.model)}

        created = self._insert(
            sql.insert.format(self.model, msg, self._placeholders(len(params))),
            [params[k] for k in self.base_model.required_fields]
        )
        return {'error': 0, 'created': self._fetchall(
            sql.select_param.format(self.model, ' '.join(self._prepare_args(created[0])))
        )}

    def delete(self, params):
        params.pop('token', 0)
        args = ['%s=\'%s\'' % (k, v) for k, v in params.items()]

        self.cursor.execute(
            sql.select_param.format(self.model, ' AND '.join(args))
        )
        target = self.cursor.fetchall()
        if not target:
            return {'error': 1, 'message': 'object does not exists'}

        if int(target[0]['is_deleted']) == 1:
            return {'error': 1, 'message': 'object already deleted'}

        self.cursor.execute(
            sql.delete.format(self.model, ' AND '.join(args))
        )

        target[0]['is_deleted'] = 1
        return {'error': 0, 'deleted': self._prepare_data(target[0])}

    def update(self, params):
        ids = params.pop('id')
        args = ', '.join(self._prepare_args(params))

        self.cursor.execute(
            sql.select_param.format(self.model, 'id=%s' % ids)
        )
        exists = self.cursor.fetchall()
        if not exists:
            return {'error': 1, 'message': '{} does not exist'.format(self.model)}

        self.cursor.execute(sql.update.format(self.model, args, ids))
        updated = self.cursor.fetchall()

        self.cursor.execute(
            sql.select_param.format(self.model, 'id=%s' % ids)
        )
        updated = self.cursor.fetchall()
        return {'error': 0, 'updated': self._prepare_data(updated[0])}

    def all(self):
        return self._fetchall(sql.select_all.format(self.model))

    def filter(self, params):
        params.pop('token', 0)
        status, msg = self._check_params(params)

        return self._fetchall(
            sql.select_filter.format(msg, self.model, ' AND '.join(self._prepare_args(params)))
        )

    def get(self, params):
        params.pop('token', 0)
        status, msg = self._check_params(params)

        return self._fetchall(
            sql.select_param.format(self.model, ' AND '.join(self._prepare_args(params)))
        )

    def execute(self, stmt, args):
        self.cursor.execute(stmt, args)

    def run(self, stmt, args):
        self.cursor.execute(stmt, args)
        return self._prepare_bulk_data(self.cursor.fetchall())

    def get_coords(self, user_id):
        self.cursor.execute(sql.get_coords2, [int(user_id)])
        return self._prepare_bulk_data(self.cursor.fetchall())
