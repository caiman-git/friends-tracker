import settings
from db import get_cursor, get_test_cursor


def initial_import():
    cursor = get_cursor()

    with open(settings.FIXTURE_PATH, 'r') as sql:
        cursor.execute(sql.read())


def create_test_db():
    cursor = get_test_cursor()

    with open(settings.TEST_FIXTURE_PATH, 'r') as sql:
        cursor.execute(sql.read())
