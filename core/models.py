import sys

sys.path.append('..')

import settings
from db import manager, get_cursor, get_fields_with_types
import datetime

MODELS = {}

__all__ = [
    'Session',
    'Group',
    'Friends',
    'Member',
    'Zone',
    'Coordinate',
    'User'
]


def registered_model(origin):
    """
    Models classes decorator.
    Provide features to register and setting up the models.

    :param origin: model class instance
    :type origin: type
    :rvalue type
    """

    name = origin.__name__.lower()

    origin.objects = manager()
    origin.objects.model = name
    origin.set_up(name)
    origin.objects.base_model = origin

    MODELS[name] = origin

    return origin


class Model(object):
    """
    Abstract base models class. Provide base methods to access and manage
    database information.
    """

    fields = None
    objects = None
    required_fields = None

    @classmethod
    def set_up(cls, name):
        """
        Getting model fields names and types from database tables.

        :param name: table name
        :type name: basestring
        """
        for field in get_fields_with_types(name):
            cls.fields[field['field']] = field['type']

    @classmethod
    def get_fields(cls):
        """
        Getting model fields [needed to create object] represented as basestring
        """
        return ', '.join(cls.required_fields)

    @classmethod
    def updates(cls, params, condition, timestamp, deleted=False):
        return cls.objects.updates(params, condition, timestamp, deleted)

    @classmethod
    def create(cls, params):
        return cls.objects.create(params)

    @classmethod
    def update(cls, params):
        return cls.objects.update(params)

    @classmethod
    def all(cls):
        return cls.objects.all()

    @classmethod
    def get(cls, params):
        return cls.objects.get(params)

    @classmethod
    def filter(cls, params):
        return cls.objects.filter(params)

    @classmethod
    def delete(cls, params):
        return cls.objects.delete(params)


class ZoneGroup(Model):
    required_fields = [
        'id',
        'group_id',
        'zone_id'
    ]
    fields = {
        'id': '',
        'group_id': '',
        'zone_id': '',
        'created_at': '',
        'updated_at': '',
        'is_deleted': ''
    }
    excluded_fields = []

    objects = manager()
    objects.model = 'zone_group'

    @classmethod
    def updates(cls, params, condition, timestamp, deleted=False):
        zones = Zone.objects.get({'user_id': params['user_id']})
        zones_ids = '({})'.format(', '.join(map(str, [i['id'] for i in zones])))

        updates_created = 'SELECT * FROM "zone_group" WHERE zone_id IN {0} AND is_deleted = 0 AND created_at >= \'{1}\';'.format(zones_ids, datetime.datetime.fromtimestamp(float(timestamp)))
        updates_updated = 'SELECT * FROM "zone_group" WHERE zone_id IN {0} AND is_deleted = 0 AND updated_at >= \'{1}\' AND created_at < \'{1}\';'.format(zones_ids, datetime.datetime.fromtimestamp(float(timestamp)))
        updates_deleted = 'SELECT * FROM "zone_group" WHERE zone_id IN {0} AND is_deleted = 1 AND updated_at >= \'{1}\' AND created_at < \'{1}\';'.format(zones_ids, datetime.datetime.fromtimestamp(float(timestamp)))

        if deleted:
            return cls.objects.run(updates_deleted, [])

        created = cls.objects.run(updates_created, [])
        updated = cls.objects.run(updates_updated, [])

        return created, updated

@registered_model
class Session(Model):
    """
    User session object representation
    """
    required_fields = [
        'token',
        'user_id',
        'expire_date'
    ]
    fields = {}
    excluded_fields = []

    @classmethod
    def create(cls, user):
        params = {
            'token': settings.hash(user['id'], settings.now_time()),
            'user_id': user['id'],
            'expire_date': int(settings.month())
        }
        return super(Session, cls).create(params)

    @classmethod
    def check_token(cls, params):
        session = cls.objects.get({'token': params.get('token', 0), 'user_id': params.get('user_id', 0)})
        if not session:
            return False
        if session[0]['expire_date'] < settings.now_time() or session[0]['alive'] != 1:
            return False
        return True

    @classmethod
    def check_session(cls, user_id):
        session = cls.objects.get({'user_id': user_id})
        if not session:
            return False
        if session[0]['expire_date'] < settings.now_time() or session[0]['alive'] != 1:
            return False
        return session[0]['token']


@registered_model
class Group(Model):
    required_fields = [
        'owner',
        'title',
    ]
    fields = {}
    excluded_fields = []

    @classmethod
    def delete(cls, params):
        gid = params.pop('group_id')
        params['id'] = gid
        return cls.objects.delete(params)


@registered_model
class Friends(Model):
    required_fields = [
        'inviter',
        'invited',
        'confirmed'
    ]
    fields = {}
    excluded_fields = []

    @classmethod
    def delete(cls, params):
        inviter = params['user_id']
        invited = params['friend_id']

        friend = cls.get({'inviter': inviter, 'invited': invited})
        deleted_user = invited
        if not friend:
            deleted_user = inviter
            inviter, invited = invited, inviter

        friend = cls.get({'inviter': inviter, 'invited': invited})

        if not friend:
            return {'error': 1, 'message': 'relation does not exists'}
        return {'error': 0, 'deleted': cls.objects.delete({'inviter': inviter, 'invited': invited})}

    @classmethod
    def prepare_params(cls, params):
        referal = params.get('referal_code', None)
        if not referal:
            return False, {'error': 1, 'message': 'messing referal_code'}
        referal = User.objects.get({'referal_code': referal})
        if not referal:
            return False, {'error': 1, 'message': 'referal user does not exists'}

        inviter = referal[0]['id']
        referal_code = params['referal_code']
        invited = params['user_id']

        return True, {'inviter':inviter, 'invited':invited, 'confirmed': 1}

    @classmethod
    def add(cls, params):
        status, params = cls.prepare_params(params)

        if not status:
            return params

        return {'error': 0, 'added': cls.create(params)}


@registered_model
class Member(Model):
    required_fields = [
        'user_id',
        'group_id'
    ]
    fields = {}
    excluded_fields = []


@registered_model
class Zone(Model):
    required_fields = [
        'user_id',
        'color',
        'title',
        'tl_longitude',
        'tl_latitude',
        'br_longitude',
        'br_latitude'
    ]
    fields = {}
    excluded_fields = []

    @classmethod
    def add_groups(cls, params):
        groups = map(int, params['groups'][1:-1].split(', '))
        stmt = 'INSERT INTO "zone_group" (group_id, zone_id) VALUES (%s, %s);'

        for group in groups:
            cls.objects.execute(stmt, [group, params['id']])
        return groups

    @classmethod
    def get_groups(cls, params):
        stmt = 'SELECT * FROM "zone_group" WHERE zone_id = %s';
        return cls.objects.run(stmt, [params['id']])

    @classmethod
    def create(cls, params):
        groups = None

        if params.get('groups', None):
            groups = params['groups']
            del params['groups']

        created = cls.objects.create(params)

        if not created:
            return {'error': 1, 'message': 'fail'}

        if groups:
            if created.get('created', None):
                groups = cls.add_groups({'id': created['created'][0]['id'], 'groups': groups})
                created['groups'] = groups

        return created

    @classmethod
    def update(cls, params):
        groups = params.get('groups', None)

        if groups:
            del params['groups']
            groups = map(int, groups[1:-1].split(', '))

            stmt = 'INSERT INTO "zone_group" (group_id, zone_id) VALUES (%s, %s);'
            stmt_del = 'DELETE FROM "zone_group" WHERE zone_id = %s;'

            cls.objects.execute(stmt_del, [params['id']])

            for group in groups:
                cls.objects.execute(stmt, [group, params['id']])

        updated = cls.objects.update(params)
        updated['updated']['groups'] = groups

        return updated

@registered_model
class Coordinate(Model):
    required_fields = [
        'id',
        'user_id',
        'latitude',
        'longitude'
    ]
    fields = {}
    excluded_fields = []

    @classmethod
    def activate(cls, params):
        params['active'] = '1'
        cls.objects.update(params)


@registered_model
class User(Model):
    required_fields = [
        'login',
        'password',
        'referal_code'
    ]
    fields = {}
    excluded_fields = []

    @classmethod
    def delete(cls, params):
        return {'error': 1, 'message': 'illegal action'}

    @classmethod
    def _prepare(cls, params):
        params['password'] = settings.hash([params['password']])
        params['referal_code'] = settings.refcode([params['login']])
        return params

    @classmethod
    def register(cls, params):
        user = cls.objects.get({'login':params['login']})
        if user:
            return {'error': 1, 'message': 'user already exist'}

        user = cls.objects.create(cls._prepare(params))

        if isinstance(user, dict):
            user = user.get('created')

        session = Session.create(user[0])
        Coordinate.objects.create({
            'id': user[0]['id'],
            'user_id': user[0]['id'],
            'latitude': 0.0,
            'longitude': 0.0,
        })
        return {'user': user[0], 'token': session['created'][0]['token']}

    @classmethod
    def findmyfriends(cls, params):
        user_id = params['user_id']

        created, _, __ = User.get_friends(user_id, 0)

        friends = [friend['id'] for friend in created]

        coords = []
        for ids in friends:
            c = Coordinate.objects.get({'id': ids})[0]
            coords.append({'id': c['id'], 'latitude': c['latitude'], 'longitude': c['longitude']})
        return {'error':0, 'coordinates': coords}

    @classmethod
    def diff(self, first, second):
        return [elem for elem in first if elem not in second]

    @classmethod
    def clean(self, lst):
        return [elem for elem in lst if int(elem.get('updated_at')) > int(elem.get('created_at'))]

    @classmethod
    def find(cls, params):
        referal_code = params.get('referal_code', None)

        if not referal_code:
            return {'error': 1, 'message': 'missing referal code'}

        user = cls.objects.get({'referal_code': referal_code})

        if not user:
            return {'error': 1, 'message': 'user does not exists'}

        return {'error': 0, 'user': user[0]}

    @classmethod
    def get_friends(cls, user_id, timestamp):
        friends_created = []
        friends_updated = []
        friends_deleted = []

        a, b = Friends.objects.updates({'inviter': user_id}, 'created_at', timestamp)
        friends_created += a
        friends_updated += b
        friends_deleted += Friends.objects.updates({'inviter': user_id}, 'updated_at', timestamp, deleted=True)

        a, b = Friends.objects.updates({'invited': user_id}, 'created_at', timestamp)
        friends_created += a
        friends_updated += b
        friends_deleted += Friends.objects.updates({'invited': user_id}, 'updated_at', timestamp, deleted=True)

        user_created = []
        user_updated = []
        user_deleted = []

        for i in friends_created:
            user_created += User.objects.get({'id': i.get('inviter')})
            user_created += User.objects.get({'id': i.get('invited')})

        for i in friends_updated:
            user_updated += User.objects.get({'id': i.get('inviter')})
            user_updated += User.objects.get({'id': i.get('invited')})

        for i in friends_deleted:
            user_deleted += User.objects.get({'id': i.get('inviter')})
            user_deleted += User.objects.get({'id': i.get('invited')})

        created = [u for u in user_created if u['id'] != int(user_id)]
        updated = [u for u in user_updated if u['id'] != int(user_id)]
        deleted = [u for u in user_deleted if u['id'] != int(user_id)]

        return created, user_updated, user_deleted


    @classmethod
    def updates(cls, params):
        user_id = params['user_id']
        timestamp = params['timestamp']

        updates = {
            'created': {},
            'updated': {},
            'deleted': {},
        }

        group_created, group_updated = Group.objects.updates({'owner': user_id}, 'created_at', timestamp)
        group_deleted = Group.objects.updates({'owner': user_id}, 'updated_at', timestamp, deleted=True)

        groups = group_created + group_updated + group_deleted
        gid = {group.get('id') for group in groups}

        members_created = []
        members_updated = []
        members_deleted = []

        for group_id in gid:
            a, b = Member.objects.updates(
                {'group_id': group_id}, 'created_at', timestamp
            )
            members_created += a
            members_updated += b
            members_deleted += Member.objects.updates(
                {'group_id': group_id}, 'updated_at', timestamp, deleted=True
            )

        members = members_created + members_updated + members_deleted
        fid = {friend.get('id') for friend in members}

        friends_created, friends_updated, friends_deleted = cls.get_friends(user_id, timestamp)

        fc = []
        for i in friends_created:
            if i['id'] != int(user_id):
                fc.append(i)
        fu = []
        for i in friends_updated:
            if i['id'] != int(user_id):
                fu.append(i)

        fd = []
        for i in friends_deleted:
            if i['id'] != int(user_id):
                fd.append(i)

        try:
            zone_created, zone_updated = Zone.objects.updates({'user_id': user_id}, 'created_at', timestamp)
            zone_deleted = Zone.objects.updates({'user_id': user_id}, 'updated_at', timestamp, deleted=True)

            zone_group_created, zone_group_updated = ZoneGroup.updates({'user_id': user_id}, 'created_at', timestamp)
            zone_group_deleted = ZoneGroup.updates({'user_id': user_id}, 'updated_at', timestamp, deleted=True)
        except Exception as e:
            zone_group_created = []
            zone_group_updated = []
            zone_group_deleted = []

        coordinate = Coordinate.objects.get({'user_id': user_id})

        updates['created']['group'] = group_created
        updates['updated']['group'] = group_updated
        updates['deleted']['group'] = group_deleted

        updates['created']['members'] = members_created
        updates['updated']['members'] = members_updated
        updates['deleted']['members'] = members_deleted

        updates['created']['friends'] = fc
        updates['updated']['friends'] = fu
        updates['deleted']['friends'] = fd

        updates['created']['zones'] = zone_created
        updates['updated']['zones'] = zone_updated
        updates['deleted']['zones'] = zone_deleted

        updates['created']['zone_group'] = zone_group_created
        updates['updated']['zone_group'] = zone_group_updated
        updates['deleted']['zone_group'] = zone_group_deleted

        # updates['coordinate'] = coordinate

        updates['timestamp'] = settings.now_time()

        return updates

    @classmethod
    def login(cls, params):
        user = cls.objects.get(cls._prepare(params))
        if not user:
            return {'error': 1, 'message': 'user does not exists'}
        token = Session.check_session(user[0]['id'])
        if not token:
            session = Session.create(user[0])
            token = session[0]['token']
        return {'user': user[0], 'token': token}

    @classmethod
    def get_coords(cls, params):
        return {'error': 0, 'coords': cls.objects.get_coords(params['user_id'])}
