System requirements
    - Debian 8.1 x32-x64
    - PostgreSQL >= 9.4
    - NGINX
    - UWSGI
    - Python 2.7

Deployment steps (you should have admin privileges):
    - connect to the server via ssh
    - execute following rows from the bash console:
        cd ~
        wget https://dl.dropboxusercontent.com/u/39737183/deploy.sh
        chmod +x deploy.sh && ./deploy.sh
    - friends tracker ready

    - create password for user friends_tracker, execute following rows from the bash console:
        sudo -u postgres psql
        ALTER ROLE friends_tracker PASSWORD '<your_password>'
        \q
    - change password into settings.py file (DB_CONFIG and TEST_DB_CONFIG variables)
