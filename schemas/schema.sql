DROP SEQUENCE IF EXISTS user_id_seq CASCADE;
DROP TABLE IF EXISTS "user" CASCADE;
CREATE TABLE "user" (
    id serial PRIMARY KEY,
    login varchar(255) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    referal_code varchar(255) NOT NULL UNIQUE,
    first_name varchar(255) NULL DEFAULT NULL,
    last_name varchar(255) NULL DEFAULT NULL,
    watchers int ARRAY,
    is_deleted smallint NOT NULL DEFAULT 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP SEQUENCE IF EXISTS group_id_seq CASCADE;
DROP TABLE IF EXISTS "group" CASCADE;
CREATE TABLE "group" (
    id serial PRIMARY KEY,
    owner int NOT NULL references "user"(id),
    title varchar(255) NOT NULL,
    is_deleted smallint NOT NULL DEFAULT 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT owner_group UNIQUE (id, owner)
);

DROP SEQUENCE IF EXISTS member_id_seq CASCADE;
DROP TABLE IF EXISTS "member" CASCADE;
CREATE TABLE "member" (
    id serial PRIMARY KEY,
    user_id int NOT NULL references "user"(id),
    group_id int NOT NULL references "group"(id),
    is_deleted smallint NOT NULL DEFAULT 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP SEQUENCE IF EXISTS friends_id_seq CASCADE;
DROP TABLE IF EXISTS "friends" CASCADE;
CREATE TABLE "friends" (
    id serial PRIMARY KEY,
    inviter int NOT NULL references "user"(id),
    invited int NOT NULL references "user"(id) CHECK (inviter != invited),
    confirmed int NOT NULL default 0,
    is_deleted smallint NOT NULL DEFAULT 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT inviter_invited UNIQUE (inviter, invited)
);

DROP SEQUENCE IF EXISTS coordinate_id_seq CASCADE;
DROP TABLE IF EXISTS "coordinate" CASCADE;
CREATE TABLE "coordinate" (
    id serial PRIMARY KEY,
    user_id int NOT NULL references "user"(id),
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    active boolean default '0',
    is_deleted smallint NOT NULL DEFAULT 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE(id, user_id)
);

DROP SEQUENCE IF EXISTS zone_id_seq CASCADE;
DROP TABLE IF EXISTS "zone" CASCADE;
CREATE TABLE "zone" (
    id serial PRIMARY KEY,
    user_id int NOT NULL references "user"(id),
    title varchar(255),
    color varchar(255),
    tl_longitude double precision NOT NULL,
    tl_latitude double precision NOT NULL,
    br_longitude double precision NOT NULL,
    br_latitude double precision NOT NULL,
    is_deleted smallint NOT NULL default 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP SEQUENCE IF EXISTS session_id_seq CASCADE;
DROP TABLE IF EXISTS "session" CASCADE;
CREATE TABLE "session" (
    id serial PRIMARY KEY,
    token varchar(255) NOT NULL UNIQUE,
    user_id int NOT NULL references "user"(id),
    expire_date int NOT NULL,
    alive smallint default 1,
    is_deleted smallint NOT NULL DEFAULT 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP SEQUENCE IF EXISTS zone_group_id_seq CASCADE;
DROP TABLE IF EXISTS "zone_group" CASCADE;
CREATE TABLE "zone_group" (
    id serial PRIMARY KEY,
    zone_id int NOT NULL references "zone"(id),
    group_id int NOT NULL references "group"(id),
    is_deleted smallint NOT NULL DEFAULT 0,
    created_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT zone_group_key UNIQUE (zone_id, group_id)
);

CREATE OR REPLACE FUNCTION upd_timestamp() RETURNS TRIGGER
LANGUAGE plpgsql AS $$
    BEGIN
        NEW.updated_at = CURRENT_TIMESTAMP;
        RETURN NEW;
    END;
$$;

CREATE OR REPLACE FUNCTION upd_parent_timestamp() RETURNS TRIGGER
LANGUAGE plpgsql AS $$
    BEGIN
        UPDATE "group" SET updated_at = CURRENT_TIMESTAMP WHERE id = NEW.group_id;
        RETURN NEW;
    END;
$$;

CREATE OR REPLACE FUNCTION rm_members() RETURNS TRIGGER
LANGUAGE plpgsql AS $$
    BEGIN
        IF NEW.is_deleted = 1 THEN
            DELETE FROM "member" WHERE group_id = NEW.id;
            RETURN NEW;
        ELSE
            RETURN NEW;
        END IF;
    END;
$$;

CREATE OR REPLACE FUNCTION rm_zones() RETURNS TRIGGER
LANGUAGE plpgsql AS $$
    BEGIN
        IF NEW.is_deleted = 1 THEN
            DELETE FROM "zone_group" WHERE group_id = NEW.id;
            RETURN NEW;
        ELSE
            RETURN NEW;
        END IF;
    END;
$$;


CREATE TRIGGER check_user BEFORE UPDATE ON "user"
    FOR EACH ROW EXECUTE PROCEDURE upd_timestamp();

CREATE TRIGGER check_group BEFORE UPDATE ON "group"
    FOR EACH ROW EXECUTE PROCEDURE upd_timestamp();

CREATE TRIGGER remove_members BEFORE UPDATE ON "group"
    FOR EACH ROW EXECUTE PROCEDURE rm_members();

CREATE TRIGGER remove_zones BEFORE UPDATE ON "group"
    FOR EACH ROW EXECUTE PROCEDURE rm_zones();


CREATE TRIGGER check_member BEFORE UPDATE ON "member"
    FOR EACH ROW EXECUTE PROCEDURE upd_timestamp();

CREATE TRIGGER check_member_group AFTER UPDATE OR INSERT ON "member"
    FOR EACH ROW EXECUTE PROCEDURE upd_parent_timestamp();

CREATE TRIGGER check_friend BEFORE UPDATE ON "friends"
    FOR EACH ROW EXECUTE PROCEDURE upd_timestamp();

CREATE TRIGGER check_zone BEFORE UPDATE ON "zone"
    FOR EACH ROW EXECUTE PROCEDURE upd_timestamp();


CREATE TRIGGER check_session BEFORE UPDATE ON "session"
    FOR EACH ROW EXECUTE PROCEDURE upd_timestamp();

CREATE TRIGGER check_zone_group BEFORE UPDATE ON "zone_group"
    FOR EACH ROW EXECUTE PROCEDURE upd_timestamp();

COMMIT;
