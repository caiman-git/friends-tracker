#! /bin/bash

apt-get update
apt-get -y upgrade
apt-get -y install sudo

aptitude -y install nginx-full
aptitude -y install uwsgi uwsgi-plugin-python
aptitude -y install python-setuptools python-virtualenv
aptitude -y install python-dev libpq-dev
aptitude -y install git

easy_install pip
pip install --upgrade pip

pip install virtualenvwrapper

mkdir -p /var/www
cd /var/www
mkdir venv

aptitude -y install postgresql

git clone git@bitbucket.org:caiman-git/friends-tracker.git
git fetch --all
git checkout dev
git pull

mv friends-tracker/ friends_tracker/

cd /var/www/friends_tracker
mkdir logs
chmod -R 777 logs

sudo rm /etc/postgresql/*.*/main/pg_hba.conf
cp /var/www/friends_tracker/configurations /etc/postgresql/*.*/main/pg_hba.conf
service postgresql restart

sudo -u postgres createuser -P -d -l -U -e friends_tracker
sudo -u postgres createdb -E UTF8 -e friends_tracker -O friends_tracker
sudo -u postgres createdb -E UTF8 -e test_database -O friends_tracker

mkvirtualenv friends_tracker
pip install -r .meta/packages

ln -s ~/.bashrc /var/www/friends_tracker/configurations/bash
ln -s /etc/nginx/sites-available/default /var/www/friends_tracker/configurations/nginx
cp /var/www/friends_tracker/configurations/uwsgi.yaml /etc/uwsgi/apps-enabled/friends_tracker.yaml
ln -s /etc/uwsgi/apps-enabled/friends_tracker.yaml /var/www/friends_tracker/configurations/uwsgi.yaml

service uwsgi restart
service nginx restart
